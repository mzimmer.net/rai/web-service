package net.mzimmer.rai.rai.adapter.active

import assertk.assertThat
import assertk.assertions.contains
import assertk.assertions.hasSize
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import assertk.assertions.isInstanceOf
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.json
import com.github.kittinunf.fuel.core.FuelManager
import io.kotlintest.specs.StringSpec
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import io.micronaut.test.extensions.kotlintest.MicronautKotlinTestExtension.getMock
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import net.mzimmer.rai.rai.adapter.passive.micronaut.data.PayerEntity
import net.mzimmer.rai.rai.adapter.passive.micronaut.data.PayerRepository
import java.io.ByteArrayInputStream
import java.util.Locale
import java.util.UUID

@MicronautTest
class EnterReceiptControllerTest(
    private val payerRepository: PayerRepository,
    private val server: EmbeddedServer
) : StringSpec({
    "should have active payers by name" {
        val mock = getMock(payerRepository)

        val searchResponse = Flowable.just(
            PayerEntity(
                id = UUID.fromString("00000000-0000-0000-0000-000000000001"),
                name = "Alice",
                isActive = true
            ),
            PayerEntity(
                id = UUID.fromString("00000000-0000-0000-0000-000000000002"),
                name = "Bob",
                isActive = false
            )
        )

        every { mock.findByNameIlike("search-term") } answers { searchResponse }

        val fm = FuelManager()
        fm.basePath = server.uri.toASCIIString()
        val get = fm.get("/enter-receipt/payer?query=search-term")

        val result = fm.client.executeRequest(get)
        val status = HttpStatus.valueOf(result.statusCode)
        assertThat(status).isEqualTo(HttpStatus.OK)

        assertThat(result.headers.keys).contains(HttpHeaders.CONTENT_TYPE.toLowerCase(Locale.ROOT))
        assertThat(result.headers[HttpHeaders.CONTENT_TYPE]).hasSize(1)
        val contentType = MediaType(result.headers[HttpHeaders.CONTENT_TYPE].first())

        assertThat(contentType.type).isEqualTo("application")
        assertThat(contentType.subtype).isEqualTo("json")
        assertThat(contentType.parameters).isEmpty()

        val parse = Parser.default().parse(ByteArrayInputStream(result.data))
        assertThat(parse).isInstanceOf(JsonObject::class)
        val body: JsonObject = parse as JsonObject

        assertThat(body).isEqualTo(json {
            obj(
                "data" to obj(
                    "payers" to obj(
                        "00000000-0000-0000-0000-000000000001" to "Alice"
                    )
                )
            )
        })
    }
}) {
    @MockBean(PayerRepository::class)
    fun payerDBRepository(): PayerRepository = mockk()
}