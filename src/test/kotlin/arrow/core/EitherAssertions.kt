package arrow.core

import arrow.core.Either.Left
import arrow.core.Either.Right
import assertk.Assert
import assertk.assertions.support.fail

fun <A, B> Assert<Either<A, B>>.isRight(): Assert<B> =
    transform { actual ->
        when (actual) {
            is Right<B> -> actual.b
            is Left<A> -> {
                fail("...".right(), actual)
                throw IllegalStateException()
            }
        }
    }

fun <A, B> Assert<Either<A, B>>.isLeft(): Assert<A> =
    transform { actual ->
        when (actual) {
            is Left<A> -> actual.a
            is Right<B> -> {
                fail("...".left(), actual)
                throw IllegalStateException()
            }
        }
    }