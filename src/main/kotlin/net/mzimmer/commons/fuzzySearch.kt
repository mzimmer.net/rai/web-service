package net.mzimmer.commons

fun fuzzyMatch(searchTerm: String): (String) -> Boolean =
    { target ->
        searchTerm.toCharArray().fold(
            initial = Acc(
                matchingSoFar = true,
                searchFromPosition = 0
            )
        ) { acc, currentChar ->
            if (acc.matchingSoFar) {
                fuzzyFindFirstSimple(target, currentChar, acc.searchFromPosition)
                    ?.let { Acc(true, it) }
                    ?: Acc(false, 0)
            } else {
                acc
            }
        }.matchingSoFar
    }

private class Acc(val matchingSoFar: Boolean, val searchFromPosition: Int)

private fun fuzzyFindFirstSimple(target: String, currentChar: Char, startIndex: Int): Int? =
    naturalOrEmpty(
        target.indexOf(
            char = currentChar,
            startIndex = startIndex,
            ignoreCase = true
        )
    )

private fun naturalOrEmpty(value: Int): Int? = if (value >= 0) value else null