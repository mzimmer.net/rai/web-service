package net.mzimmer.commons

import arrow.core.Either
import arrow.core.left
import arrow.core.right

fun <T> (() -> T).errorAsLeft(): Either<Throwable, T> =
    try {
        this().right()
    } catch (t: Throwable) {
        t.left()
    }