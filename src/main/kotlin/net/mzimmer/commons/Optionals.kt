package net.mzimmer.commons

import arrow.core.Option
import java.util.Optional

fun <A> Optional<A>.asOption(): Option<A> =
    map { Option.just(it) }.orElse(Option.empty())

fun <A> Option<A>.asJavaOptional(): Optional<A> =
    fold({ Optional.empty() }) { Optional.of(it) }