package net.mzimmer.commons

fun <T, U> Iterable<T>.filterBy(filter: (U) -> Boolean, f: (T) -> U): Iterable<T> =
    this.filter { filter(f(it)) }

fun <K, V, U> Map<K, V>.filterBy(filter: (U) -> Boolean, f: (Map.Entry<K, V>) -> U): Map<K, V> =
    this.filter { filter(f(it)) }