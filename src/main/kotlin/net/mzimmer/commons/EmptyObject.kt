package net.mzimmer.commons

import com.fasterxml.jackson.annotation.JsonValue

class EmptyObject private constructor() {
    companion object {
        val emptyObject: EmptyObject = EmptyObject()

        sealed class EmptyObjectOr<out A> {
            data class NonEmpty<A>(val value: A) : EmptyObjectOr<A>() {
                @JsonValue
                private fun jsonValue(): A = value
            }

            object EmptyObject : EmptyObjectOr<Nothing>() {
                @JsonValue
                private fun jsonValue(): net.mzimmer.commons.EmptyObject = emptyObject
            }

            companion object {
                fun <A> A.nonEmpty(): EmptyObjectOr<A> = NonEmpty(this)
                fun <A> emptyObject(): EmptyObjectOr<A> = EmptyObject

                fun <K, V> Map<K, V>.emptyAsNonEmpty(): EmptyObjectOr<Map<K, V>> =
                    if (isNotEmpty()) nonEmpty() else emptyObject()
            }
        }
    }
}