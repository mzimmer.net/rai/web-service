package net.mzimmer.rai.jackson

import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.micronaut.context.annotation.Factory
import javax.inject.Singleton

@Factory
internal class JacksonFactory {
    @Singleton
    fun jacksonKotlinModule(): KotlinModule = KotlinModule()
}