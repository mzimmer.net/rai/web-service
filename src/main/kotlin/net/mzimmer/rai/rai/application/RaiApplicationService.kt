package net.mzimmer.rai.rai.application

import io.reactivex.Maybe
import io.reactivex.Single
import net.mzimmer.rai.rai.domain.Payer

interface RaiApplicationService {
    fun findPayer(command: Payer.Step.Find): Maybe<Payer.Step.Find.Result>
    fun findPayers(command: Payer.Step.FindMany): Single<Payer.Step.FindMany.Result>
    fun addNewPayer(command: Payer.Step.AddNew): Single<Payer.Step.AddNew.Result>
    fun updatePayer(command: Payer.Step.Update): Maybe<Payer.Step.Update.Result>
}