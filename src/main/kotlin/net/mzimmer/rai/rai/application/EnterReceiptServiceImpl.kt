package net.mzimmer.rai.rai.application

import io.reactivex.Single
import net.mzimmer.rai.rai.application.EnterReceiptService.PayerSearchCommand
import net.mzimmer.rai.rai.domain.Payer
import net.mzimmer.rai.rai.domain.Payer.Step.FindMany.Result
import net.mzimmer.rai.rai.port.passive.PayerRepository
import javax.inject.Singleton

@Singleton
class EnterReceiptServiceImpl internal constructor(
    private val payerRepository: PayerRepository
) : EnterReceiptService {
    override fun payerSearch(command: PayerSearchCommand): Single<PayerSearchCommand.Result> =
        payerRepository
            .findMany(Payer.Step.FindMany.ByNameFuzzy(searchTerm = command.searchTerm))
            .map(Result::payers)
            .map { it.filter { entry -> entry.value.activeState == Payer.ActiveState.Active } }
            .map { PayerSearchCommand.Result(payers = it) }
}