package net.mzimmer.rai.rai.application

import io.reactivex.Maybe
import io.reactivex.Single
import net.mzimmer.rai.rai.domain.Payer
import net.mzimmer.rai.rai.port.passive.PayerRepository
import javax.inject.Singleton

@Singleton
class RaiApplicationServiceImpl internal constructor(
    private val payerRepository: PayerRepository
) : RaiApplicationService {
    override fun findPayer(command: Payer.Step.Find): Maybe<Payer.Step.Find.Result> =
        payerRepository.find(command)

    override fun findPayers(command: Payer.Step.FindMany): Single<Payer.Step.FindMany.Result> =
        payerRepository.findMany(command)

    override fun addNewPayer(command: Payer.Step.AddNew): Single<Payer.Step.AddNew.Result> =
        payerRepository.addNew(command)

    override fun updatePayer(command: Payer.Step.Update): Maybe<Payer.Step.Update.Result> =
        payerRepository.update(command)
}