package net.mzimmer.rai.rai.application

import io.reactivex.Single
import net.mzimmer.rai.rai.domain.Payer

interface EnterReceiptService {
    fun payerSearch(command: PayerSearchCommand): Single<PayerSearchCommand.Result>

    data class PayerSearchCommand(val searchTerm: String) {
        data class Result(val payers: Map<Payer.Id, Payer.WithoutId>)
    }
}