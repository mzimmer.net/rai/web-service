package net.mzimmer.rai.rai.adapter.passive

import io.reactivex.Maybe
import io.reactivex.Single
import net.mzimmer.rai.rai.adapter.passive.micronaut.data.PayerEntity
import net.mzimmer.rai.rai.domain.Payer
import net.mzimmer.rai.rai.domain.Payer.Step.Update.Activate
import net.mzimmer.rai.rai.domain.Payer.Step.Update.Deactivate
import net.mzimmer.rai.rai.domain.Payer.Step.Update.Name
import net.mzimmer.rai.rai.port.passive.PayerRepository
import javax.inject.Singleton

@Singleton
class MicronautDataPayerRepository internal constructor(
    private val payerRepository: net.mzimmer.rai.rai.adapter.passive.micronaut.data.PayerRepository
) : PayerRepository {
    override fun find(step: Payer.Step.Find): Maybe<Payer.Step.Find.Result> =
        when (step) {
            is Payer.Step.Find.ById -> payerRepository.findById(step.id.value)
        }
            .map(PayerEntity::toDomain)
            .map { Payer.Step.Find.Result(payer = it) }

    override fun findMany(step: Payer.Step.FindMany): Single<Payer.Step.FindMany.Result> =
        when (step) {
            Payer.Step.FindMany.All -> payerRepository.findAll()
            is Payer.Step.FindMany.ByNameFuzzy -> payerRepository.findByNameIlike(step.searchTerm)
        }
            .map(PayerEntity::toDomain)
            .reduce<Map<Payer.Id, Payer.WithoutId>>(emptyMap()) { payers, payer -> payers.plus(payer.pair()) }
            .map { Payer.Step.FindMany.Result(payers = it) }

    override fun addNew(step: Payer.Step.AddNew): Single<Payer.Step.AddNew.Result> =
        payerRepository
            .save(
                PayerEntity(
                    name = step.name.value,
                    isActive = true
                )
            )
            .map(PayerEntity::toDomain)
            .map { Payer.Step.AddNew.Result(payer = it) }

    override fun update(step: Payer.Step.Update): Maybe<Payer.Step.Update.Result> =
        payerRepository
            .updateById(step.id().value) {
                when (step) {
                    is Name -> it.name = step.name.value
                    is Activate -> it.isActive = true
                    is Deactivate -> it.isActive = false
                }
            }
            .map(PayerEntity::toDomain)
            .map { Payer.Step.Update.Result(payer = it) }

}