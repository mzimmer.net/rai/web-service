package net.mzimmer.rai.rai.adapter.active

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponse.ok
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.reactivex.Single
import net.mzimmer.commons.EmptyObject.Companion.EmptyObjectOr
import net.mzimmer.commons.EmptyObject.Companion.EmptyObjectOr.Companion.emptyAsNonEmpty
import net.mzimmer.rai.rai.application.EnterReceiptService
import java.util.UUID

@Controller("/enter-receipt")
class EnterReceiptController internal constructor(
    private val enterReceiptService: EnterReceiptService
) {
    @Get("/store-with-categories")
    fun storeWithCategoriesSearch(@QueryValue("query") query: String): HttpResponse<Unit> = TODO()

    @Get("/payer")
    fun payerSearch(@QueryValue("query") query: String): HttpResponse<Single<PayerSearchResponseBody>> =
        ok(
            enterReceiptService
                .payerSearch(EnterReceiptService.PayerSearchCommand(searchTerm = query))
                .map { result ->
                    PayerSearchResponseBody(
                        data = PayerSearchResponseBody.Data(
                            payers = result.payers
                                .mapKeys { it.key.value }
                                .mapValues {
                                    it.value.name.value
                                }
                                .emptyAsNonEmpty()
                        )
                    )
                }
        )

    data class PayerSearchResponseBody(val data: Data) {
        data class Data(val payers: EmptyObjectOr<Map<UUID, String>>)
    }
}