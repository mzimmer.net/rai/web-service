package net.mzimmer.rai.rai.adapter.active

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpResponse.created
import io.micronaut.http.HttpResponse.notFound
import io.micronaut.http.HttpResponse.ok
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.server.util.HttpHostResolver
import io.micronaut.http.uri.UriBuilder
import io.reactivex.Single
import net.mzimmer.commons.EmptyObject
import net.mzimmer.commons.EmptyObject.Companion.EmptyObjectOr
import net.mzimmer.commons.EmptyObject.Companion.EmptyObjectOr.Companion.emptyAsNonEmpty
import net.mzimmer.commons.EmptyObject.Companion.emptyObject
import net.mzimmer.rai.rai.adapter.active.PayerController.IndexResponseBody.Data
import net.mzimmer.rai.rai.application.RaiApplicationService
import net.mzimmer.rai.rai.domain.Payer
import java.util.UUID

@Controller("/payers")
class PayerController internal constructor(
    private val raiApplicationService: RaiApplicationService,
    private val httpHostResolver: HttpHostResolver
) {
    @Get
    fun index(): HttpResponse<Single<IndexResponseBody>> =
        ok(
            raiApplicationService
                .findPayers(Payer.Step.FindMany.All)
                .map { result ->
                    IndexResponseBody(
                        data = IndexResponseBody.Data(
                            result.payers
                                .mapKeys { it.key.value }
                                .mapValues {
                                    Data.Payer(
                                        name = it.value.name.value,
                                        activeState = when (it.value.activeState) {
                                            Payer.ActiveState.Active -> "active"
                                            Payer.ActiveState.Inactive -> "inactive"
                                        }
                                    )
                                }
                                .emptyAsNonEmpty()
                        )
                    )
                }
        )

    data class IndexResponseBody(val data: Data) {
        data class Data(val payers: EmptyObjectOr<Map<UUID, Payer>>) {
            data class Payer(val name: String, val activeState: String)
        }
    }

    @Post
    fun create(@Body request: CreateRequestBody, httpRequest: HttpRequest<*>): Single<HttpResponse<EmptyObject>> =
        raiApplicationService
            .addNewPayer(
                Payer.Step.AddNew(
                    name = Payer.Name(request.data.payer.name)
                )
            )
            .map { result ->
                val httpHost = httpHostResolver.resolve(httpRequest)
                val location = UriBuilder.of(httpHost)
                    .replacePath(httpRequest.path)
                    .path(result.payer.id.value.toString())
                    .build()
                created(emptyObject, location)
            }

    data class CreateRequestBody(val data: Data) {
        data class Data(val payer: Payer) {
            data class Payer(val name: String)
        }
    }

    @Get("/{id}")
    fun get(@PathVariable id: UUID): Single<HttpResponse<GetResponseBody>> =
        raiApplicationService
            .findPayer(Payer.Step.Find.ById(id = Payer.Id(id)))
            .map<HttpResponse<GetResponseBody>> {
                ok(
                    GetResponseBody(
                        data = GetResponseBody.Data(
                            payer = it.payer
                        )
                    )
                )
            }
            .toSingle(notFound(null))

    data class GetResponseBody(val data: Data) {
        data class Data(val payer: Payer.Full)
    }

    @Put("/{id}/name")
    fun updateName(@PathVariable id: UUID, @Body request: UpdateNameRequestBody): Single<HttpResponse<EmptyObject>> =
        raiApplicationService
            .updatePayer(
                Payer.Step.Update.Name(
                    id = Payer.Id(id),
                    name = Payer.Name(request.data.payer.name)
                )
            )
            .map<HttpResponse<EmptyObject>> { ok(emptyObject) }
            .toSingle(notFound(emptyObject))

    data class UpdateNameRequestBody(val data: Data) {
        data class Data(val payer: Payer) {
            data class Payer(val name: String)
        }
    }

    @Put("/{id}/activeState")
    fun updateActiveState(
        @PathVariable id: UUID,
        @Body request: UpdateActiveStateRequestBody
    ): Single<HttpResponse<EmptyObject>> =
        raiApplicationService
            .updatePayer(
                if (request.data.payer.activeState == "active")
                    Payer.Step.Update.Activate(id = Payer.Id(id))
                else
                    Payer.Step.Update.Deactivate(id = Payer.Id(id))
            )
            .map<HttpResponse<EmptyObject>> { ok(emptyObject) }
            .toSingle(notFound(emptyObject))

    data class UpdateActiveStateRequestBody(val data: Data) {
        data class Data(val payer: Payer) {
            data class Payer(val activeState: String)
        }
    }
}