package net.mzimmer.rai.rai.adapter.passive.micronaut.data

import net.mzimmer.rai.rai.domain.Payer
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "payers")
data class PayerEntity(
    @Id
    @GeneratedValue
    @Column(
        name = "id",
        unique = true,
        nullable = false,
        insertable = false,
        updatable = false,
        columnDefinition = "uuid"
    )
    var id: UUID = UUID(0, 0),

    @Column(name = "name", nullable = false)
    var name: String = "",

    @Column(name = "is_active", nullable = false)
    var isActive: Boolean = false
) {
    fun toDomain(): Payer.Full =
        Payer.Full(
            id = Payer.Id(id),
            name = Payer.Name(name),
            activeState = if (isActive) Payer.ActiveState.Active else Payer.ActiveState.Inactive
        )
}