package net.mzimmer.rai.rai.adapter.passive.micronaut.data

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.CrudRepository
import java.util.Optional
import java.util.UUID

@Repository
abstract class PayerBlockingRepository : CrudRepository<PayerEntity, UUID> {
    fun updateById(id: UUID, update: (PayerEntity) -> Unit): Optional<PayerEntity> =
        findById(id)
            .map {
                update(it)
                this.update(it)
            }
}