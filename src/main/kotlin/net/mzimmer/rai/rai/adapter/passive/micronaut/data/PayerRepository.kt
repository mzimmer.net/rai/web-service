package net.mzimmer.rai.rai.adapter.passive.micronaut.data

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.reactive.RxJavaCrudRepository
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import java.util.UUID

@Repository
abstract class PayerRepository internal constructor(
    private val payerBlockingRepository: PayerBlockingRepository
) : RxJavaCrudRepository<PayerEntity, UUID> {
    abstract fun findByNameIlike(name: String): Flowable<PayerEntity>

    fun updateById(id: UUID, update: (PayerEntity) -> Unit): Maybe<PayerEntity> =
        Maybe.fromCallable {
                payerBlockingRepository
                    .updateById(id, update)
                    .orElse(null)
            }
            .observeOn(Schedulers.io())
}