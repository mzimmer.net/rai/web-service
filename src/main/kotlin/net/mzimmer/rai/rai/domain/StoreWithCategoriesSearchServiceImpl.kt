package net.mzimmer.rai.rai.domain

import net.mzimmer.rai.rai.domain.StoreWithCategoriesSearchService.SearchParameters
import net.mzimmer.rai.rai.domain.StoreWithCategoriesSearchService.SearchResult
import javax.inject.Singleton

@Singleton
class StoreWithCategoriesSearchServiceImpl : StoreWithCategoriesSearchService {
    override fun search(searchParameters: SearchParameters): SearchResult = TODO()
}