package net.mzimmer.rai.rai.domain

import arrow.core.Option
import net.mzimmer.rai.rai.domain.types.CategoryId
import net.mzimmer.rai.rai.domain.types.CategoryName
import net.mzimmer.rai.rai.domain.types.StoreId
import net.mzimmer.rai.rai.domain.types.StoreName

interface StoreWithCategoriesSearchService {
    fun search(searchParameters: SearchParameters): SearchResult

    data class SearchParameters(val searchTerm: SearchTerm) {
        data class SearchTerm(val value: String)
    }

    data class SearchResult(val results: Map<StoreId, StoreWithCategories>) {
        data class StoreWithCategories(
            val name: StoreName,
            val parent: Option<ParentStore>,
            val categories: Categories
        )

        data class ParentStore(
            val id: StoreId,
            val name: StoreName,
            val parent: Option<ParentStore>
        )

        data class Categories(
            val entries: Map<CategoryId, Category>
        )

        data class Category(
            val name: CategoryName,
            val parent: Option<ParentCategory>
        )

        data class ParentCategory(
            val id: CategoryId,
            val name: CategoryName,
            val parent: Option<ParentCategory>
        )
    }
}