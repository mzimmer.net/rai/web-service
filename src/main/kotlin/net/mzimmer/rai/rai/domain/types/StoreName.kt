package net.mzimmer.rai.rai.domain.types

import com.fasterxml.jackson.annotation.JsonValue

data class StoreName(@JsonValue val value: String)