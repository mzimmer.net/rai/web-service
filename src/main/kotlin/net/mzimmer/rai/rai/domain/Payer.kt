package net.mzimmer.rai.rai.domain

import java.util.UUID

object Payer {
    data class Id(val value: UUID)
    data class Name(val value: String)
    sealed class ActiveState {
        object Active : ActiveState()
        object Inactive : ActiveState()
    }

    data class Full(val id: Id, val name: Name, val activeState: ActiveState) {
        fun withoutId(): WithoutId =
            WithoutId(name, activeState)

        fun pair(): Pair<Id, WithoutId> =
            Pair(id, withoutId())
    }

    data class WithoutId(val name: Name, val activeState: ActiveState) {
        fun full(id: Id): Full =
            Full(id, name, activeState)
    }

    object Step {
        sealed class Find {
            data class ById(val id: Id) : Find()
            data class Result(val payer: Full)
        }

        sealed class FindMany {
            object All : FindMany()
            data class ByNameFuzzy(val searchTerm: String) : FindMany()
            data class Result(val payers: Map<Id, WithoutId>)
        }

        data class AddNew(val name: Name) {
            data class Result(val payer: Full)
        }

        sealed class Update {
            data class Name(val id: Id, val name: Payer.Name) : Update()
            data class Activate(val id: Id) : Update()
            data class Deactivate(val id: Id) : Update()
            data class Result(val payer: Full)

            fun id(): Id =
                when (this) {
                    is Name -> id
                    is Activate -> id
                    is Deactivate -> id
                }
        }
    }
}