package net.mzimmer.rai.rai.domain.types

import com.fasterxml.jackson.annotation.JsonValue
import java.util.UUID

data class StoreId(@JsonValue val value: UUID)