package net.mzimmer.rai.rai.domain.types

import com.fasterxml.jackson.annotation.JsonValue

data class CategoryName(@JsonValue val value: String)