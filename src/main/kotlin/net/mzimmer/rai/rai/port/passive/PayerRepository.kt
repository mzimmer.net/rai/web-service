package net.mzimmer.rai.rai.port.passive

import io.reactivex.Maybe
import io.reactivex.Single
import net.mzimmer.rai.rai.domain.Payer

interface PayerRepository {
    fun find(step: Payer.Step.Find): Maybe<Payer.Step.Find.Result>

    fun findMany(step: Payer.Step.FindMany): Single<Payer.Step.FindMany.Result>

    fun addNew(step: Payer.Step.AddNew): Single<Payer.Step.AddNew.Result>

    fun update(step: Payer.Step.Update): Maybe<Payer.Step.Update.Result>
}