package net.mzimmer.rai

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("net.mzimmer.raiwebservice")
                .mainClass(Application.javaClass)
                .start()
    }
}