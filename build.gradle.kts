plugins {

    val versions = object {
        val kotlin = "1.3.50"
        val shadow = "5.2.0"
    }

    kotlin(module = "jvm").version(versions.kotlin)
    kotlin(module = "kapt").version(versions.kotlin)
    id("org.jetbrains.kotlin.plugin.allopen").version(versions.kotlin)
    id("com.github.johnrengelman.shadow").version(versions.shadow)
    application
}

group = "net.mzimmer.rai"
version = "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://jcenter.bintray.com")
}

val developmentOnly = configurations.create("developmentOnly")

dependencies {

    val versions = object {
        val micronaut = "1.3.0.M2"
        val micronautDataProcessor = "1.0.0"
        val arrow = "0.10.2"
        val jacksonKotlinModule = "2.10.+"
        val klaxon = "5.2"
        val logback = "1.2.3"

        val kotlintest = "3.3.2"
        val mockk = "1.9.3"
        val assertk = "0.20"
        val fuel = "2.2.1"
    }

    // Kotlin

    implementation(kotlin(module = "stdlib"))
    implementation(kotlin(module = "reflect"))

    // Micronaut

    implementation(platform(notation = "io.micronaut:micronaut-bom:${versions.micronaut}"))
    implementation(group = "io.micronaut", name = "micronaut-runtime")
    implementation(group = "io.micronaut", name = "micronaut-http-server-netty")

    kapt(platform(notation = "io.micronaut:micronaut-bom:${versions.micronaut}"))
    kapt(group = "io.micronaut", name = "micronaut-inject-java")
    kapt(group = "io.micronaut", name = "micronaut-validation")

    kapt(
        group = "io.micronaut.data",
        name = "micronaut-data-processor",
        version = versions.micronautDataProcessor
    )
    implementation(
        group = "io.micronaut.data",
        name = "micronaut-data-hibernate-jpa",
        version = versions.micronautDataProcessor
    )
    runtimeOnly(group = "io.micronaut.configuration", name = "micronaut-jdbc-hikari")
    runtimeOnly(group = "com.h2database", name = "h2")

    // Dependencies

    implementation(group = "io.arrow-kt", name = "arrow-core-data", version = versions.arrow)
    // TODO: Depend explicitly on RxJava (or RxKotlin)
    implementation(
        group = "com.fasterxml.jackson.module",
        name = "jackson-module-kotlin",
        version = versions.jacksonKotlinModule
    )

    runtimeOnly(group = "ch.qos.logback", name = "logback-classic", version = versions.logback)

    // Testing

    kaptTest(platform(notation = "io.micronaut:micronaut-bom:${versions.micronaut}"))
    kaptTest(group = "io.micronaut", name = "micronaut-inject-java")

    testImplementation(platform(notation = "io.micronaut:micronaut-bom:${versions.micronaut}"))
    testImplementation(group = "io.micronaut.test", name = "micronaut-test-kotlintest")
    testImplementation(group = "io.kotlintest", name = "kotlintest-runner-junit5", version = versions.kotlintest)
    testImplementation(group = "io.mockk", name = "mockk", version = versions.mockk)
    testImplementation(group = "com.willowtreeapps.assertk", name = "assertk-jvm", version = versions.assertk)
    testImplementation(group = "com.github.kittinunf.fuel", name = "fuel", version = versions.fuel)
    testImplementation(group = "com.beust", name = "klaxon", version = versions.klaxon)
}

allOpen.annotation("io.micronaut.aop.Around")

application {
    mainClassName = "net.mzimmer.rai.Application"
}

tasks {

    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.javaParameters = true
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.javaParameters = true
    }

    shadowJar {
        mergeServiceFiles()
    }

    test {
        classpath += developmentOnly
        useJUnitPlatform()
    }

    (run) {
        classpath += developmentOnly
        jvmArgs = listOf(
            "-noverify",
            "-XX:TieredStopAtLevel=1",
            "-Dcom.sun.management.jmxremote"
        )
    }
}